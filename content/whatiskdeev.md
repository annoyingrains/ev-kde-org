---
title: What is KDE e.V.
menu:
  main:
    parent: info
    weight: 1
---

KDE e.V. is a registered non-profit organization that represents
the <a href="https://kde.org">KDE Project</a> in legal and financial
matters.

<blockquote>
  <p>
  "The Association's purpose is the promotion and distribution of free desktop
  software in terms of free software, and the program package 'K Desktop
  Environment (KDE)' in particular, to promote the free exchange of knowledge
  and equality of opportunity in accessing software as well as education,
  science and research."
  </p>

  <p style="text-align:right"><em>Quote from the articles of association of the KDE
  e.V.</em></p>
</blockquote>

The Association aids in creating and distributing KDE by securing cash,
hardware, and other donations, then using donations to aid KDE development and
promotion.

The Association was originally founded to create a legal entity capable of
representing the KDE Project in the
<a href= "https://kde.org/community/whatiskde/kdefreeqtfoundation">KDE
Free Qt Foundation</a>. The growing popularity of KDE also made it
necessary to establish a thin organizational layer that is capable of handling
<em>all</em> legal and financial interests of the KDE developers. In 1997 KDE
e.V. was registered as an association under German law. The "e.V." stands for
"eingetragener Verein" which means "registered association".

## About KDE

KDE is a diverse, worldwide community that creates Plasma, a graphical desktop environment for computers, mobile phones and other devices. The KDE community offers a variety of Free and Open Source applications for multiple platforms, including GNU/Linux, Windows, macOS, and Android. KDE software is translated into more than 100 languages, and hundreds of contributors are constantly working on making it more usable, accessible, attractive, and reliable.

From file managers, email clients, and text editors to multimedia players and digital graphics tools, KDE applications help users control their digital lives regardless of their background and skills. The KDE community strives to create software that is simple by default, and powerful when needed.

Ever since its beginnings in 1996, the KDE community has been committed to bringing Free software to as many platforms and devices as possible. In collaboration with various communities and commercial partners, the KDE community aims to increase the adoption of Free software among the general public. KDE puts the users' safety and privacy first, and develops innovative solutions to their problems. The KDE software is Free, and always will be.

The KDE community is dedicated to giving everyone an opportunity to contribute. KDE regularly participates in mentorship programs aimed at young students from different backgrounds. With the help of KDE e.V., its legal and financial representative, the KDE community organizes developer sprints, meetings, and community events. The most important event is called Akademy - a yearly week-long conference comprising talks, hackathons, and community discussions that bring together KDE users and contributors from all walks of life.

## What we do

KDE e.V.'s (from now on &quot;the e.V.&quot;) primary purpose is
to support KDE. KDE is a community of developers, translators, writers
and users. The e.V. supports the community through the following
long-term activities:

+ The e.V. holds property for the good of the entire community.
This property includes the KDE trademarks, hardware and networking
equipment and cash money.
+ Makes the property available to individual contributors
as needed in order to support KDE. This includes paying for
travel to meetings, subsidizing events, purchasing individual hardware
and offsetting other costs.
+ Organizes events including code sprints, developer meetings
and the yearly KDE World Conference Akademy.
+ Approaches sponsors and supporting members for resources
in order to carry out these activities.

## How we do it

KDE e.V. is an association under German law. We have <a href="/members/">members</a>
who elect a <a href="/corporate/board/">board of directors</a> who are responsible
for the operation of the association and who ensure that the e.V. meets its goals.

The activities of the members of KDE e.V. are the backbone of actually
supporting KDE development. This includes promotional activity, writing articles
and organizing events.

