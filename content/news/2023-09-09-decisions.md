---
title: 'KDE e.V. re-elects board members'
date: 2023-09-09 1:30:00
layout: post
noquote: true
slug: decisions
categories: [Board]
---

KDE e.V. held its [annual general meeting](/reports/2023-de/) (German) online
and at the KDE e.V. offices in Berlin. During the AGM, elections for three vacancies on the board of directors were held. Members Aleix Pol, Eike Hein, and Lydia Pintscher were (re)elected to the [board](/corporate/board/).

There were no public votes or decisions in the third quarter of 2023 other than those described in the minutes of the AGM.
