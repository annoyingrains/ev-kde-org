---
title: "Mitgliederversammlung KDE e.V. 2022"
layout: page
---

Die Mitgliederversammlung findet am Montag, den 3. Oktober 2022 um 10:00 MESZ (CEST) in Edifici Vèrtex, Universitat Politècnica de Catalunya, Plaça Eusebi Güell 6, 08034 Barcelona, Spanien statt.

Zu Beginn der Veranstaltung sind 44 Mitglieder anwesend. 37 nicht anwesende Mitglieder haben einen Vertreter benannt. Als Gast nimmt Joseph P. De Veaugh-Geiss, ein freier Mitarbeiter des Vereins für das Blaue Engel Projekt, an der Veranstaltung teil.

## Tagesordnung

1. Begrüßung
2. Wahl des Versammlungsleiters
3. Bericht des Vorstands
   1. Bericht über Aktivitäten
   2. Bericht des Schatzmeisters
   3. Bericht der Rechnungsprüfer
   4. Entlastung des Vorstandes
4. Bericht der Vertreter und Arbeitsgruppen des Vereins
   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der KDE Free Qt Working Group
   3. Bericht der System Administration Working Group
   4. Bericht der Community Working Group
   5. Bericht der Financial Working Group
   6. Bericht der Advisory Board Working Group
   7. Bericht der Fundraising Working Group
5. Wahl des Vorstands
6. Wahl der Rechnungsprüfer
7. Wahl der Vertreter in der KDE Free Qt Foundation
8. Verschiedenes

## Protokoll

Alle Berichte wurden den Mitgliedern im Vorfeld per Email zugesandt und am 02.10.2022 zusätzlich auf der durch den Verein organisierten Konferenz [Akademy](https://akademy.kde.org/2022) öffentlich vorgestellt.

### Begrüßung und Wahl des Versammlungsleiters

Um 10:32 eröffnet der Vorsitzende des Vorstands, Aleix Pol i Gonzàlez, die Versammlung.

Harald Sitter wird per Akklamation zum Versammlungsleiter gewählt.

Joseph P. De Veaugh-Geiss wird als Gast von der Versammlung zugelassen bei einer Enthaltung.

Der Versammlungsleiter stellt fest, dass die Einladung zur Mitgliederversammlung ordnungsgemäß und fristgerecht - per Emailversand am 21.08.2022  mit vorläufiger und erneut am 19.09.2022 mit finaler Tagesordnung (ohne Änderung zur vorläufigen) - erfolgt ist.

Die Tagesordnung wird per Akklamation bestätigt und es wurden keine weiteren Punkte angebracht.

Der Versammlungsleiter benennt David Redondo als Protokollant und stellt fest, dass das satzungsgemäße Quorum erfüllt ist.

### Bericht des Vorstands

#### Bericht über Aktivitäten

Der aktuelle Vorstand besteht aus Aleix Pol i Gonzàlez (Vorsitzender), Eike Hein (Schatzmeister und Stellvertreter des Vorsitzenden), Lydia Pintscher (Stellvertreterin des Vorsitzenden), Adriaan de Groot und Neofytos Kolokotronis.
Mit dieser Mitgliederversammlung enden die Amtszeiten von Adriaan de Groot und Neofytos Kolokotronis.

8 neue aktive Mitglieder wurden in den Verein seit der letzten Mitgliederversammlung aufgenommen, eines verließ ihn jedoch wieder.

Der Verein hat erneut einige Fördermitglieder verloren. Im Vergleich zu 71 im letzten Jahr sind es in diesem Jahr nur noch 48.
Es wurde mit Github ein neuer Weg eröffnet dem Verein Spenden zukommen zu lassen, was schon von 45 Personen und Organisationen genutzt wird.

In Tuxedo Computers wurde ein neuer Patron gefunden der somit auch Teil des Advisory Boards ist, mit Kontent GmbH wurde ein Supporter verloren.

Der Verein hat eine Angestellte und sieben freie Mitarbeiter. Petra Gillert ist als Assistenz des Vorstands angestellt, Aniqa Khokhar und Paul Brown arbeiten als Marketing-Berater und  Adam Szopa als Projekt-Koordinator. Neu hinzugekommen sind Dina Nouskali als Event-Koordinator, Joseph P. De Veaugh-Geiss und Lana Lutz für das Blauer Engel Projekt und Ingo Klöcker als App Store Engineer.
Die freien Mitarbeiter Allyson Alexandrou verließen den Verein im Dezember 2021, Frederik Schwarzer im September 2021 und Carl Schwan im Juni 2021.
Der Vorstand versucht die ausstehenden Stellen für Software Entwicklung, Dokumentation und Hardware Integration zu füllen.

Im letzten Jahr gab es wieder einige Sprints neben Online Sprints. Der Vorstand ermutigt weitere Sprints zu organisieren. Auch wurden einige Konferenzen wieder besucht.
Letzes Jahr fand dass 25te Jubiläum von KDE statt, zu diesem Anlass wurden einige Aktivitäten organisiert.
Für Akademy 2023 steht der Ausrichtungsort fest. Es sollen Sprints und Treffen der Mitglieder unterstützt werden.

Mit dem Kdenlive Projekt zusammen wird erprobt Spenden für einzelne Projekte zu sammeln.

Die KDE Gemeinschaft hast sich neue Ziele gegeben, die unterstützt werden sollen.

Es wird gefragt ob der volle Umfang der Spenden an das Kdenlive Projekt weitergeleitet wird - der Verein behält einen Anteil für den Verwaltungsaufwand ein.

#### Bericht des Schatzmeisters

Eike Hein erläutert den Finanzbericht, den er mit Hilfe der Financial Working Group und des Vorstands erstellt hat.

##### Finanzjahr 2021

Die Finanzsituation ist ähnlich zu 2020 und der Verein befindet sich in einer stabilen finanziellen Lage. Es gab einen Anstieg von sowohl Einnahmen als auch Ausgaben.
Es wurde wieder das Ziel verfehlt mehr Geld auszugeben, aufgrund der Pandemiesituation gab es sehr wenige Reisen, auch war es schwerer als geplant einige ausgeschriebene Stellen zu füllen.
Anfang 2021 gab es eine Großspende von PINE64, auch wurden Sponsorzahlungen für Linux App Summit 2020 erhalten.

Eike stellt danach einige Details der Einnahmen und Ausgaben an Hand von grafischen Auswertungen dar.

##### Finanzplan 2022

Auf der Einnahmenseite wird ein moderater Anstieg der Einnahmen erwartet, zwar sinken die Einnahmen aus dem Google Summer of Code Projekt,
jedoch erhält das Blauer Engel Projekt eine höhere Fördersumme. Die Auflösung der KDE League Inc. schreitet voran, deren Kapital dem Verein zufallen wird.

Es wird ein starker Anstieg an Ausgaben erwartet da wieder mehr Reisen stattfinden und mehr Leute eingestellt wurden bzw. werden sollen.

Anschließend werden Fragen aus der Mitgliedschaft zu den Berichten beantwortet.

#### Bericht der Kassenprüfer
Der Bericht der Kassenprüfer wurde vor der Versammlung den Mitglieder bereits am 30.07.2022 per Email zugesandt.

Die Kassenprüfer haben am 23.07.2022 die Prüfung bei einem Treffen im Büro des Vereins mit Lydia Pintscher und Petra Gillert durchgeführt und haben Einsicht in die Bücher genommen.
Es wird eine ordnungsgemäße Buchführung attestiert. Alle Fragen der Kassenprüfer wurden zu deren Zufriedenheit beantwortet.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2021.

#### Entlastung des Vorstands

Der Versammlungsleiter bittet um Meldung, ob Einwände gegen die Entlastung bestehen. Es gibt keine Meldungen.

Die Abstimmung wird jetzt durchgeführt. Die Mitglieder des Vorstandes nehmen an dieser Abstimmung nicht teil. Das Abstimmungsergebnis lautet:

* Für die Entlastung:     **75**
* Gegen die Entlastung:    **0**
* Enthaltungen:            **0**
* Abstimmungsteilnehmer:  **75**

Mit diesem Ergebnis ist der Vorstand für die vergangene Berichtsperiode entlastet.

### Bericht der Vertreter und Arbeitsgruppen des Vereins

Hier werden von den einzelnen Gruppen lediglich Ergänzungen gegeben, die nicht für die Öffentlichkeit bestimmt sind und daher nicht in den vorab versandten Berichten und der öffentlichen Präsentation enthalten sind.

#### Bericht der Vertreter in der KDE Free Qt Foundation/ der KDE Free Qt Working group

Albert Astals Cid berichtet als Vertreter des KDE e.V. aus der KDE Free Qt Foundation.

Albert is nun Vorsitzender der Foundation. Lars Knoll, einer der Verteter der Qt Company, hat die Foundation verlassen, ein neuer Vertreter wurde noch nicht benannt.

Albert erläutert einige Aktivitäten im Detail und beantwortet Fragen zu diesen.

#### Community Working Group

Die Community Working Group beantwortet Fragen aus der Mitgliedschaft zu ihrer allgemeinen Vorgehensweise und spezifischen Vorfällen.

#### Andere Gruppen

Die anderen Gruppen haben über die in den öffentlich zugänglichen Informationen keine weiteren Punkte zu berichten.

### Wahl des Vorstandes
Die Amtszeiten der Vorstände Adriaan de Groot und Neofytos Kolokotronis enden mit der Versammlung. Neofytos und Adriaan stellen sich für eine Wiederwahl zur Verfügung. Weitere Kandidaten sind Andy Betts und Nathaniel Graham. Es gibt keine weiteren Kandidaten.

Die Kandidaten stellen sich kurz vor und beantworten Fragen der Mitglieder.

Es wird geheim abgestimmt. Jeder Teilnehmer hat zwei Stimmen. Die abgegebenen Stimmen ergeben folgendes Wahlergebnis:

* Adriaan de Groot:        **56**
* Neofytos Kolokotronis:   **43**
* Andy Betts:              **14**
* Nathaniel Graham:        **48**
* Enthaltungen             **1**
* Stimmen:                 **162**

Adriaan und Nathaniel nehmen die Wahl an.

### Wahl der Rechnungsprüfer

Von den beiden bisherigen Rechnungsprüfern steht Andreas Cord-Landwehr für eine Wiederwahl zur Verfügung, Ingo Klöcker nicht. Des weiteren stellt sich Thomas Baumgart zur Wahl. Es gibt keine weiteren Kandidaten.

Die Mitglieder stimmen über beide Kandidaten zusammen ab. Die abgegebenen Stimmen ergeben folgendes Wahlergebnis:

* Zustimmung:        **78**
* Enhaltungen:       **3**
* Wahlteilnehmer:    **81**

Andreas nimmt nach Rückfrage des Versammlungsleiters die Wahl an. Thomas hat bereits im Vorfeld schriftlich erklärt die Wahl anzunehmen im Falle, dass er gewählt wird.

### Wahl der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer und Albert Astals Cid stehen für eine Wiederwahl zur Verfügung. Es gibt keine weiteren Kandidaten.

Die Mitglieder stimmen über beide Kandidaten zusammen ab. Die abgegebenen Stimmen ergeben folgendes Wahlergebnis:

* Zustimmung:                 **76**
* Enhaltungen:                **5**
* Wahlteilnehmer:             **81**

Beide Kandidaten nehmen die Wahl nach Rückfrage durch den Versammlungsleiter an.

### Verschiedenes
 
Es werden keine weiteren Punkte diskutiert.

### Schluss der Versammlung
Harald Sitter beendet die Versammlung um 12:46.
<br />
<br />
<br />
<br />
<br />
<br />
David Redondo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Harald Sitter<br/>
(Protokollant)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(Versammlungsleiter)<br/>
04.10.2022
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
04.10.2022
