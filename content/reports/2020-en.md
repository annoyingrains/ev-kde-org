---
title: "General Assembly KDE e.V. 2020"
layout: page
---

The General Assembly took place monday 7 September 2020 at 13:00 CEST.

The meeting was held in a password protected online video conference with integrated chat, using an open source tool called [BigBlueButton](https://bigbluebutton.org/). The agenda and presentations were presented and shared using an open source tool called [OpenSlides](https://openslides.com/de). OpenSlides was also used to register the presence of e.V. members and record the proxies. The list of participants was exported from OpenSlides for documentation purposes. 94 members were present, among them 2 non-voting supporting members. 3 absentee members nominated representatives on their behalf. No non-member was present.

Both systems (BigBlueButton and OpenSlides) ran on KDE servers during the meeting. All members received access to these tools in advance of the meeting.

## Agenda

1. Greetings
2. Election of the meeting chairperson
3. Reports of the board
   1. Report on activities
   2. Report from the treasurer
   3. Report from the auditors of accounting
   4. Relief of the board
4. Report from representatives and working groups
   1. Report from the representatives of the KDE Free Qt Foundation
   2. Report from the System Administration Working Group
   3. Report from the Community Working Group
   4. Report from the Financial Working Group
   5. Report from the Advisory Board Working Group
   6. Report from the Fundraising Working Group
5. Election of the board
6. Election of the auditors of accounting
7. Election of representatives to the KDE Free Qt Foundation
8. Miscellaneous discussion

## Minutes

### Welcome and election of chairperson for the meeting

At 13:00 the meeting was opened by the president of the board, Aleix Pol. He thanked the organisers of the General Assembly.

Frederik Gladhorn was elected as chairperson for the meeting by general acclaim.

The chairperson noted that invitations to the General Assembly were sent out in the proper fashion and in a timely manner. There were no objections.

The agenda for the meeting was acknowledged by general acclaim.

The chairperson appointed Cornelius Schumacher as record keeper of the minutes.

The chairperson noted that the attendance of the meeting constituted a quorum of the general membership.

### Report of the board

The status reports covered the activities of the e.V. since the last General Assembly. The report had been presented already on 05-09-2020 at the [Akademy](https://akademy.kde.org/2020) conference, organised by the e.V.

The current board consists of Aleix Pol i Gonzàlez (chairperson of the board), Eike Hein (treasurer and deputy chairperson of the board), Lydia Pintscher (deputy chairperson of the board), Adriaan de Groot and Neofytos Kolokotronis.

The terms of office for Aleix, Lydia and Eike concluded with this General Assembly.

11 new active members have joined the e.V. since the last General Assembly.

The e.V has now 84 supporting members as compared to 95 last year. Work on the infrastructure to manage the supporting members is ongoing. To this end, a new consultant was engaged.

One new company joined as associate member. One company terminated its associate membership, because it was acuqired and subsequently re-evaluated its position.

The Advisory Board was enlarged, consisting of representatives of KDE patrons and elected community partners.

The e.V. has one direct employee and 4 freelance contractors. Petra Gillert is a direct employee in the capacity of assistant to the Board, Aniqa Khokhar and Paul Brown work as marketing consultants, Adam Szopa as project coordinator, and Allyson Alexandrou as event coordinator.

The community report for 2019 concerning the activities of the community and the e.V. was presented.

Evolution of KDE: the next round of community goals is already ongoing. Additional structuring, documentation and support for the goalsetting and implementation processes within the e.V is taking place with the help of Adam Szopa.

Goals from the first round of goals continue to receive support.

Roles and responsibilities of the working groups of the e.V. have been systematically charted and clarified in collaboration with the working groups. The results have already been presented to the e.V. members and will be published after Akademy.

There were a few ideas to alter the statutes of the e.V and develop these further. The board is canvassing possible improvements. The process is still in an early phase and must be implemented carefully without any rush. The board asked for proposals.

KDE will celebrate its 25th birthday on 14 Oktober 2021. The board asked for ideas on how to use and celebrate the occasion.

#### Last year's goals of the board

* Organising Akademy 2020: the event took place. Due to the Corona situation, it was hosted as a virtual event. 600 participants have registered. The event is considered a success.
* Investing in the growth of our community members: more trainings have taken palce, such as implicit bias training. The extent of trainings must be increased further. The offer of access to professional libraries did not receive much interest. Ideas for future activities are welcome.
* Clarification of the role of KDE in environmental sustainability: travel has the most major direct impact on the environment. Becuase of the Corona situation, this has been greatly reduced. Another approach is being pursued via a project in the context of the Blauer Engel.
* Effectiveness of the board's network: clarification of roles and responsibilities of the working groups has concluded.
* Purposeful and effective spending of resources: in two areas, freelance consultants were hired and investments were made in the infrastructure for virtual events.
* Establishing processes: processes were improved, professionalised, and documented such as the process for onboarding new colleagues.
* Forging parternsips with other likeminded organisations: the Linux App Summit was successfully organised in collaboration with the GNOME project.

Two thirds of goals were achieved, other goals were obviated by changing circumstances.

#### Key themes for next year

* Coming together again: to the extent that health regulations will permit, in-person gatherings will held again. A call was made for hosts of Akademy 2021.
* Financial sustainability: infrastructure for associate members will be improved and the associate membership programme will be made more attractive.
* Sustainable employment: steps will be made to enable more people to make a living off of KDE related work.
* Supporting an open source friendly hardware ecosystem: the partnernetwork will be increased to advance open source friendly hardware.
* Imitating and strengthening what works: areas of KDE which are particularly successful will be identified, and lessons learned will be applied to other areas of KDE e.V.

### Report from the Tresurer

Eike Hein presents the Report from the Treasurer, which was written with the help of the Financial Working Group and the other members of the board.

#### Fiscal Year 2019

Income in 2019 was stable. There was no fundraising campaign, because of two large donations in the previous year, from the Pineapple Fund and Handshake Foundation.
Despite that, the overall income-situation is very stable. Eike emphasises that this is good for the association.

A few invoices for supporting membership in 2019 were paid in 2020.

Sponsoring for Akademy is developing well. The conference is financially stable.

Expenses were higher than income. This was planned, in order to satisfy the requirement to use income for the goals of the association
and to make use of the donations from previous here. Projected expenses were even higher, but money from the budget
for, e.g. the community-goals processes, was not requested or spent.

Events make up the largest part of regular spending for the association. The Linux App Summit is a new large event which
is co-funded and co-hosted by KDE e.V. The Randa meetings from previous years were displaced by the Linux App Summit.
A new Summit is planned.

Spending has continually grown in the past years.

Bookkeeping and the accounts were problematic this year, largely due to changes in personnel at the accountants office.
Steps are being taken to improve the situation.

Income is largely stable, especially in the corporate supporting memberships. Together, the supporting memberships provide most of the income.

Three-quarters of all expenses are for employees and contractors, and Akademy and other events.

#### Budget 2020

The budget-plan for 2020 was published late in june, because it needed modifications to account for the Corona-crisis.
The focus of the budget has moved from travel support to personnel costs. Additionally, the infrastructure
for virtual evens is being expanded.

The overall strategy of the budget, which is to increase the support for central areas of the community, has not changed.
Liquidity is very good. Financial reserves have increased. Cash reserves are now 650000 EUR, part of which is earmarked.

The community-goals team has its own earmarked funding and budget. The team has a clear mandate from the community for its spending.

#### Current State 2020

Income from donations is once again stable. One new corporate supporting member (Kontent GmbH) has joined
the association, while one Patron has left.

Akademy turns a profit, even though sponsorship levels were lowered for the virtual event.
The focus for Akademy is quality content for the community, not for realising a surplus.

Spending on contractors has increased markedly.

Spending on travel support is expected to rise sharply in future once travel becomes possible again.

The financial reserves held by the association allow for several years of current activity.
It is important to build up alternative financing for all activities.
The work done by the Fund Raising Working Group and other parts of the community supports that.
There are perhaps three years funding (with no income) available for building up
income sources and formulating sustainable plans. The association intends to
experiment and learn from various funding sources. The board indicates that
this will be done in the next time-frame.

### Report from the Auditors of Accounting

The auditors of accounting, Andreas Cord-Landwehr and Ingo Klöcker present their report.
Members have already received this report by email.

The auditors of accounting have done the accounting in a meeting at the KDE e.V. offices
with Lydia and Petra and have examined the books.
The accounting is of high quality -- higher even than last year.
Only with close examination are any issues to be found,
and unimportant issues at that. The auditors of accounting
are very satisfied with the accounting.

The auditors of accounting recommend to the membership that
the board be relieved for the financial year 2019.

### Relief of the Board

The chairperson explains the meaning of "relief of the board".
The chairperson asks if there are questions or comments regarding the relief of the board.
There are none.

The chairperson organizes a poll in Big Blue Button. The board does not participate in the poll.
Results of the poll are 77 yes-votes, zero no-votes and 8 abstentions.

The chairperson asks if there are questions or comments about the poll results. There are none.
The board is declared relieved.

### Other Reports

Reports from the working groups and the representatives have already presented during
the public portion of Akademy, and have been sent to all members by email.

#### Report of the Representatives to the KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer and Martin Konold present the report from the
representatives for KDE e.V. in the KDE Free Qt Foundation.

There are, once again, negotiations regarding updates to the agreement that
ensures the availability of Qt for Free Software development.

One issue is the **in**compatibility between the requirements of
the commercial license for Qt and its use as Open Source software.
The largest differences of opinion have been resolved,
but there are still a few nitpicks.

Olaf asks for a straw poll, if this topic should be pursued further.
83 members (of 94 present) enter an opinion. 44 state this should
be pursued further, 6 say "no" and there are 33 abstentions.

During Q&A the membership emphasises that the priority is to be
the availability of Qt for the development of Free Software.
Olaf states that this is ensured in any case, since it is the founding
principle of the KDE Free Qt Foundation.

Olaf and Marin suggest that a Working Group should be founded
in order to support the work of the representatives of to the KDE Free Qt Foundation.

#### Working Groups

The Financial Working Group consists of members of the associant who
are elected for a period of two years.
Some members have completed their term. The Financial Working Group asks
for new members to present themselves as candidate.

The Advisory Board Working Group has as goal to take care of the relationship
between KDE and its partners and to be a point of contact.
This year the group has arranges a phone-meeting so that the members
of the advisory board can be informed of important developments within
KDE.
OpenUK is added as a new member of the advisory board.

The Community Working Group receives a fairly large number of
questions from people that wish to join the community.
However, the CWG's purpose is conflict resolution within the community.
It is suggested to change the name of the Working Group to
better reflect its purpose.

### Votes

There are three votes scheduled for today. The votes will be done online, using the online service
[Belenios](https://www.belenios.org) which enables secure, secret and auditable voting.

Jeff Mitchell, who leads the technical use of the voting tools, explains the voting procedure.
The three votes will take place simultaneously.
After the candidates have been named, the list of candidates will be closed
and an email sent to each member with voting privileges who is present at this time.
The email contains secure access codes for the voting system.
Members can vote by following the information provided in the email,
and confirm that their vote is counted in the voting.

#### Candidates

There are three open positions for the board, since the three-year term of
three board members has expired.
All three board members are candidates for re-election. There are no other candidates.

Therefore the candidates for the vote for board positions are:

* Aleix Pol Gonzalez
* Eike Hein
* Lydia Pintscher

The auditors of accounting from last year announce their candidacy for re-election.
There are no other candidates.

Therefore the candidates for the vote for auditors of accounting are:

* Andreas Cord-Landwehr
* Ingo Klöcker

As representatives for KDE e.V. in the KDE Free Qt Foundation announce their candidacy for re-election.
There are no other candidates.

Therefore the candidates for the vote for representatives to the KDE Free Qt Foundation are:

* Martin Konold
* Olaf Schmidt-Wischhöfer

#### Voting Results

There are 89 voters registered electronically.

Results of board election:

* Aleix Pol Gonzalez: 89 Votes
* Eike Hein: 88 Votes
* Lydia Pintscher: 86 Votes

All three candidates have been re-elected. All three candidates accept the election.

Results of the election of the auditors of accounting:

* Andreas Cord-Landwehr: 88 Votes
* Ingo Klöcker: 86 Votes

Both candidates have been re-elected. Both candidates accept the election.

Results of the election of KDE e.V.representatives to the KDE Free Qt Foundation:

* Martin Konold: 89 Votes
* Olaf Schmidt-Wischhöfer: 84 Votes

Both candidates have been re-elected. Both candidates accept the election.

An error in determining the list of valid voters has prevented four of the members of the association, who are present, from obtaining an electronic voting registration.
All other members who are present have voted.
The four members who are affected each state that they do not object to the voting result.
No replacement vote is needed and the elections are declared valid.

### Miscellaneous

There is a discussion about progress in the context of the KDE Free Qt Foundation.

A request for a straw poll for a Working Group to support the KDE Free Qt Foundation shows:
45 for, 9 against, no abstentions.

### Closing

The general assembly is closed by the chairperson at 17:30.

The general assembly was not interrupted by sound- or video-problems.
