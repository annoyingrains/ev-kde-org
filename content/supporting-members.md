---
title: "Supporting Members"
menu:
  main:
    parent: organization
    weight: 4
description: Become a supporting member and help fund KDE activities
scssFiles:
- /css/main.scss
---

If your company or organization would like to become a supporting member please
have a look at the
[information how to become a supporting member of the KDE e.V.](/getinvolved/supporting-members/)

Individuals wishing to support KDE financially should consider to [Join the Game](https://relate.kde.org/civicrm/contribute/transact?reset=1&id=9).

Currently the supporting members of the KDE e.V. are:

<h2>Patrons of KDE
<img src="/images/patron.svg" class="kbadge" />
</h2>

<div class="logo-list img-patrons mb-5 kPatrons">
  <img class="canonical" alt="Canonical" />
  <img class="google" alt="Google" />
  <img class="suse" alt="SUSE" />
  <img class="qt" alt="Qt Group" />
  <img class="blue-systems" src="/images/logos/blue-systems.png" alt="Blue Systems" />
  <img class="slimbook" src="/images/logos/slimbook.svg" alt="Slimbook" />
  <img class="tuxedo" src="/images/logos/tuxedo.svg" alt="TUXEDO Computers" />
  <img class="kfocus" alt="Kubuntu Focus" />
  <img class="gnupg" alt="g10 Code" />
</div>



<h2>Supporters of KDE
<img src="/images/supporter_small.png" class="kbadge" />
</h2>

<div class="logo-list img-supporters">
  <img src="/images/supportingmembers/kdab.png" alt="KDAB" />
  <img src="/images/supportingmembers/basyskom.png" alt="Basyskom" />
  <img class="enioka" src="/images/supportingmembers/enioka.svg" alt="Enioka Haute Couture" />
</div>
