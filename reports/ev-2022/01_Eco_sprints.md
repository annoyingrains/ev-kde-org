<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Eco/1_1_groupphoto.webp" alt="Smiling community members after a productive day." />
    </figure>
</div>

[KDE Eco](https://eco.kde.org/), the team of community members committed to making Free Software more environmentally friendly, held three sprints in 2022.

<figure class="image-right">
    <img width="100%" src="images/Sprints/Eco/1_2_GUDE.webp" alt="An electronic device in a transparent perspex box sits on a large, black table. The device (GUDE) has several cables connected to it, one going to a special power outlet. KDE Eco team members sit around the table running tests on their laptops."/>
    <figcaption>The GUDE Expert Power Control 1202.</figcaption>
</figure>

**On May 21st, 2022**, the KDE Eco community held an in-person Sprint to measure the energy consumption of free software. KDAB Berlin hosted the event and donated hardware and a new power meter. Eleven people attended, including representatives from KDE, [KDAB Berlin](https://www.kdab.com/), [Qt Company](https://www.qt.io/), [Green Coding Berlin](https://www.green-coding.org/), and [Hasso-Plattner-Institut/Universität Potsdam](https://osm.hpi.de/). The primary goal for the day was to set up two desktop computers and the [GUDE Expert Power Control 1202](https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html) power meter for measuring how much energy Free Software consumes, and the participants also worked with high-resolution [Microchip MCP39F511N](https://www.microchip.com/en-us/product/MCP39F511N) and low-resolution [Gosund SP111](https://web.archive.org/web/20201022205319/https://templates.blakadder.com/gosund_SP111_v2.html) devices to collect data.

The lab setup followed the approach described in the research paper [Sustainable software products -- Towards assessment criteria for resource and energy efficiency](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/1-s2.0-S0167739X17314188-main.pdf), and the reference systems on the systems under test (SUT) were KDE Neon User Edition and Kubuntu 22.04. The long-term vision for the lab is to automate the measurement process with remote access for FOSS developers to measure their software easily and from anywhere in the world. Participants did not work on automation at the Sprint, but the SUTs were set up for remote access using SSH and VNC. Another goal is to use exclusively Free Software in the lab, and a Python script was used to read out the measurement data from the Gude power meter.

<figure class="image-left">
    <img width="100%" src="images/Sprints/Eco/2_1_a-test-of-kde-eco-tester.webp" alt="A monitor shows the window of a basic GCompris activity. Behind, a terminal window records the power consumption of the activity."/>
    <figcaption>Running the KDE Eco Tester.</figcaption>
</figure>

**On Saturday 16th of July**, the team met up to work on the community measurement lab in Berlin. Nine participants attended, with six being in-person and three virtual. The goals of the event were to equip the lab with FOSS-based tooling and to begin measuring KDE/Free Software. While progress was made in tooling, there was no measurement of software yet.

Discussions at the event revolved around whether KDE/Free Software could reduce carbon intensity for updates and other power-hungry processes. Plans are in the pipeline to bring the larger community together to implement this across the updated systems of interested distros.

KDE Eco is working to integrate KDE's plotting software, [LabPlot](https://labplot.kde.org/), into the lab. An unexpected delay in the measurement readout was discovered during the event, which will need to be resolved before the next Sprint.

**On Saturday 27 August**, a small group met at KDAB Berlin for a follow-up Sprint to tackle the readout issue with the power meter, as described above. In total, we were 1 virtual and 4 in-person attendees (Arne, Volker, Nico, Björn, Joseph). As a result of the Sprint, I am happy to announce that the measurement lab now has a working Python script to read output from the GUDE power meter. You can download the script from the [FEEP repository](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter). A special thanks to the contribution of Arne from Green Coding Berlin (and you can read about their adventures in DC measurements in [this blog post](https://www.green-coding.org/blog/adventures-in-dc-measurement/)). So here we are: the lab should now be ready for the really exciting part of the project, measuring software!

Volker also worked on updating the notes for the Gosund SP111 power plug, which included information about switching WiFi networks and recovering devices when they no longer boot, another problem that needed solving from the last Sprint. You can follow the guide yourself [here](https://invent.kde.org/teams/eco/feep/-/blob/master/tools/gosund-sp111-notes.md). What is more, once you have the hacked power plug working, you can also [download LabPlot](https://labplot.kde.org/) and try out live plotting to see energy consumption in real time. To do this, redirect the power plug output to a CSV file (see [instructions](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html) at Volker's blog). Then, in LabPlot import the CSV file by going to *File > Add New > Live Data Source ...*. Where it says "Filter" select the *Custom* option, and under "Data Format" define the separator value used (e.g., comma, semi-colon, space). You can check that the output is correct under the "Preview" tab. If everything looks good, click *OK*. Now it is just a matter of right-clicking on the data frame window and selecting *Plot Data > xy-Curve*. Voilà!



