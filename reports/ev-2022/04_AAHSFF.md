<div style='text-align: center'>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/RYLyXFCnA0o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

[The All-American High School Film Festival (hereafter, A-AHSFF)](https://www.hsfilmfest.com/) is a week-long event hosted in New York City. Students can submit their short films in advance, and at the event, there are exhibitions and rewards for the best-judged films. There is also a tech fair, in 2022 it was on the 22nd of October. KDE community member Nataniel Farzan is one of this year’s selected entrants, and negotiated a booth for KDE at the tech fair!

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/AAHSFF/nataniel_attendee.webp" alt="Showing an attendee how to edit videos with Kdenlive." />
    </figure>
</div>

Our plan was to inform attendees about [Kdenlive](https://kdenlive.org), KDE's full-featured video-editing software, increase the userbase and, with a bit of luck, get it introduced to schools were film-making was taught.

### Booth Setup

Philip and I arrived in plenty of time, an hour before the festival started. This turned out to be good, because the venue had an extreme shortage of power outlets and none close to our booth! There was only one outlet in the whole area. I had a long extension cord, but not long enough to run all the way across the room. Happily, we didn’t need much power to run three laptops, so the people setting up the booth in front of the outlet let us plug into their power strip.

With that out of the way, we plugged my power strip into the extension cord, covered the extension cord with mats also kindly offered by the Fotocare booth to prevent a tripping hazard, and got everything set up.

[Krita](htps://krita.org) turned out to be a great addition to the demo. Students loved doodling with the pen touchscreens, and several attendees were interested in it for its ability to do animation.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/AAHSFF/krita.webp" alt="Attendees sit drawing on a tablet at the KDE booth." />
        <figcaption>Drawing using Krita at the KDE booth.</figcaption>
    </figure>
</div>

### Student Engagement

I spoke with exactly one person (a student) who knew what Kdenlive was. So I needed a proactive strategy. Across from our booth, the huge lights of the Fotocare booth could not be missed. So I used this to our advantage. As groups or individual students came to admire the lights, I would intercept them and ask simply, "Can I give you a sticker?"

After all, who doesn’t want a sticker? I gave away a *lot* of stickers this way. Most visitors would say "Yes", after which point I had their attention and could present my quick pitch. "This is for Kdenlive, free video editing software written by volunteers."

### Student Reception

To be honest, I am surprised at how well my short pitch worked. Some (say, 15%) students were just not interested, and responded with some form of "Ok, thanks for the sticker" and wandered off.

But most were at least intrigued, at which point I would launch into my second phase: "It’s totally free, it runs on Mac, Windows, and Linux, and it supports Adobe file formats. It’s running on these laptops, please feel free to have a seat and try it out." The second phrase evolved a lot over the day, trying to head off the questions I had already received, like "What do you mean, free? Is there a subscription?" and "Are you a startup?"

Nearly every attendee who heard this part of my pitch would look at the laptops and examine them for a few seconds, maybe prod the computer, and most (say, 80%) would leave with some form of "Cool! I’ll check it out." I imagine by the time their long day is over, most students will have forgotten. But hopefully, a few will find their stickers and remember to install them.

Finally, there was the rarer group of students who would actually sit down, and were *really* interested. This happened about 10 times throughout the event, but those students were really engaged. I’m pretty confident they’ll look for their sticker and install Kdenlive.

### Educator Engagement

At the same time that we were trying to engage students, I was trying to pick out and engage those adults who were not just parents wandering around after their darting children, but educators who had expressed
interest in putting Kdenlive in their classrooms. Before the event got fully underway, I started an online form to collect data from interested educators. This form also evolved throughout the event, but I intentionally kept it short and simple. Just an email entry, a few checkboxes, and a field for "anything else" (largely for our usage, so we could keep notes about them!)

We chatted with quite a few educators and everyone I chatted with put their information into my form. The overwhelming response was "Wow, this is so cool, I can send this home with my students so they can work when their school laptop (and thus, Adobe subscription) isn’t available." I remember one educator who primarily worked for a large, wealthy university, but in the summers teaching at a poorer school where he was excited to bring Kdenlive. Another educator said his school district paid for Adobe subscriptions, but that would end as soon as his students graduated, and he was excited to be able to offer them Kdenlive as a replacement.

### Postmortem

Overall, I am very happy with how the festival turned out. We reached lots of excited students, I suspect many of them will try Kdenlive shortly. Moreover, we reached many excited educators, who thought Kdenlive would fit in their classroom in one way or another.
