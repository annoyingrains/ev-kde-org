In [Season of KDE ](https://season.kde.org/)2022, seven candidates took on and completed projects that helped them learn about Open Source and also expanded their knowledge of how software is created, managed, packaged and distributed. They had the experience of creating features for applications aimed at real users, and learned about the ever-pressing need for more efficient and eco-friendly software, along with much more.

### The Projects

[Ayush Singh](https://community.kde.org/SoK/2022/StatusReport/Ayush_Singh) worked on writing a Rust wrapper for the [KConfig](https://develop.kde.org/docs/use/configuration/introduction/) KDE Framework. KConfig simplifies the process of writing values to, and reading options from an app's configuration file. Ayush's project will allow developers to use KConfig in Rust projects without having to write C++ code. Ayush wrote several [posts](https://www.programmershideaway.xyz/posts/post2/) explaining which [bindings and features](https://docs.rs/kconfig/0.1.0/kconfig/) are now complete and can be used directly in Rust.

Talking of writing apps, [Samarth Raj](https://community.kde.org/SoK/2022/StatusReport/Samarth_Raj) added a new activity to GCompris. [GCompris](https://gcompris.net/) is a suite of educational activities for children from the ages of 2 to 10, and is used widely in schools and homes all over the world. [Samarth's activity](https://invent.kde.org/education/gcompris/-/merge_requests/94) helps kids differentiate between the left and right mouse click. It does so by encouraging the child to click on 2 different animals (a horse and a monkey). By using either the left or right button on the mouse, the child can then make each of the animals go to their respective homes (a stable and a tree).

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/SoK/GCompris.png" alt="Screenshot showing a GCompris activity with horses and monkeys." />
        <br />
        <figcaption>The new activity to GCompris that helps kids differentiate between the left and right mouse click.</figcaption>
    </figure>
</div>

Samarth [relates his journey in his blog](https://samarthrajwrites.wordpress.com/2022/01/28/season-of-kde-2022/) and says that, although he had previous experience with HTML, CSS, and Javascript, he thought SoK was a great opportunity to learn about open source, and gain some confidence using [Qt/Qml](https://doc.qt.io/qt-5/qtqml-index.html).

Another new feature in an existing application is the Perspective Ellipse Assistant tool [Srirupa Datta](https://community.kde.org/SoK/2022/StatusReport/Srirupa_Datta) worked on for Krita. [Krita](https://krita.org/) is KDE's design and painting program and the ellipse assistant tool would help artists draw ellipses easier.

<figure class="image-right">
    <img width="100%" src="images/SoK/Ellipse.gif" alt="Animation of the Perspective Ellipse Assistant tool in action." />
    <br/>
    <figcaption>the Perspective Ellipse Assistant tool in action.</figcaption>
</figure>


The tool was [still at the work-in-progress stage](https://invent.kde.org/graphics/krita/-/merge_requests/1343) by the end of SoK, but has since been fully implemented.

KDE apps are not only available for Linux. In fact, many projects are making an effort to reach all users regardless of the platform they are on, and [Stefan Kowalczyk](https://community.kde.org/SoK/2022/StatusReport/Stefan_Kowalczyk) worked on improving [KDE Connect](https://kdeconnect.kde.org/) on iOS, Apple's mobile phone and tablet operating system.

iOS has the particularity that it can only display one alert at a time. This means that when KDE Connect raised multiple alerts at the same time, only one was being shown. Stefan's project queued the alerts, thereby preventing the user from losing information.

The [code](https://invent.kde.org/network/kdeconnect-ios/-/merge_requests/42) was merged. If you are an iOS user and would like to use KDE Connect on your phone, you may want [to read more about this SoK](https://fixmbr.github.io/), and follow the progress of the project.

For Stefan "\[...\] it has been a great opportunity to learn more about iOS development and work with a community-driven open source project. \[...\] SoK was the thing I needed to finally contribute to the Open Source community".

Building new applications and new features into applications is fine, but then comes the problem of delivering them to the users. Flatpak is becoming an increasingly popular way of distributing software to users and [Snehit Sah](https://community.kde.org/SoK/2022/StatusReport/Snehit_Sah) packaged several KDE applications for Flatpak and implemented [continuous integration](https://invent.kde.org/sysadmin/ci-utilities/-/merge_requests/17) for Flatpak packages.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/SoK/Flatpak.png" alt="Screenshot showing KDE's Discover app manager and several apps you can download and install." />
        <br />
        <figcaption>You can now download more packages as flatpaks.</figcaption>
    </figure>
</div>

Snehit said "I almost jumped to the ceiling when I saw the word "packaging". \[...\] I've never worked with Flatpak before, but I have a basic understanding of packaging, and it is in fact one of the things I take a lot of interest in". Snehit also remarked on how the Season of KDE was a great booster to his experience.

To find out about all the applications that Snehit updated, check out his [blog](https://snehit.dev/posts/kde/sok-2022/).

In similar news, [Suhaas Joshi](https://community.kde.org/SoK/2022/StatusReport/Suhaas_Joshi) worked on displaying the [permissions for Flatpak applications in the Discover](https://invent.kde.org/plasma/discover/-/merge_requests/282) interface. This tells users what they can expect the application to require, like read/write permissions to access the storage, or location data, and so on.

Apart from guaranteeing users' freedom and privacy, KDE strives to reduce the carbon footprint of its apps by improving their energy efficiency. [Karanjot Singh](https://community.kde.org/SoK/2022/StatusReport/Karanjot_Singh) worked with the KDE Eco team to [prepare Standard Usage Scenarios](https://eco.kde.org/blog/2022-03-03-sok22-kde-eco/) for measuring the energy consumption of various text editors and [developed a script for Kate](https://invent.kde.org/teams/eco/be4foss/-/merge_requests/1/).

Karanjot Singh remarked on how he learned a lot about working with different automation tools, and creating standard usage scenarios for different applications and frameworks, a skill that will come in handy in the future.
