From the 3rd to the 4th of April, Kenny Duffus, Kenny Coyle, Carl Schwan, and Bhushan Shah met online for the Meet Team's 2021 sprint.

Over the last year, KDE has been running BigBlueButton on meet.kde.org. Where initially, meet.kde.org was built to support Akademy 2020, it has become a very valuable community resource enabling remote meetings and events.

The main goals of this sprint were to:

* Reflect on what has worked and what can be improved.
* Determine if there are any key traits that we should add to meet.kde.org to turn it into a better, permanent community resource.
* Perform some discovery on other events using similar technologies and determine if their implementations could give us some inspiration.
* Look forward to Akademy 2021 and design the "conference experience," outline any new technology that we might need to put it together.
* Where possible, build prototypes for any new technologies that we would like to build, proving (or disproving) the concept.
