<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/KDE_PIM_Sprint_2.jpg" alt="Attendees" />
        <br />
        <figcaption>Attendees to the 2022 online PIM sprint.</figcaption>
    </figure>
</div>

From 2003 to 2012, the KDE PIM community used to meet at Osnabrück for its traditional New Year's meeting. Originating from a project to bring FOSS groupware to German public institutions, these meetings were instrumental in forming the KDE PIM community, shape the development of Kontact, and act as the birthplace for famous (and infamous) technologies, such as Akonadi.

In 2021 we wanted to revive this tradition for a one-time get-together of former and current members of the KDE PIM community. As we can't meet in person, we did this as an online afternoon meeting we called "Virtual Osnabrück".

We met on the 9th of January 2021. The purpose of the meeting was to (re)connect the community, to meet old friends, and to talk past, current, and future KDE PIM. Some of the topics we covered included:

· KDE PIM today and in the future

· History or lessons learned from the Osnabrück meetings

· What happened to project XYZ we conceived or talked about at Osnabrück?

· KDE PIM anecdotes

· What's hot in KDE PIM today?

We had the first two sessions, the first looking back to how Osnabrück came to be and what happened there between 2003 and 2012 when the KDE PIM community regularly met at Osnabrück. Bernhard showed some historical documents on how the meeting emerged from the work on adding encryption to free software email in the "Ägypten" projects and how this was followed by the bold idea of implementing free software groupware based on proven scalable email technology. This was the birth of the adequately named project "Kroupware", which later resulted in the development of Kontact and tools such as Kleopatra. We heard how this was all about the nose ;-)

The idea of KDE PIM as a sub community also was realized in Osnabrück. One of the results of the first meeting was the move of KMail from the kdenetwork module to the new kdepim module in, back then, CVS. It joined KOrganizer and KAddressbook there, and provided a natural home for Kontact, which integrated all the tools for a groupware client. The culture of Osnabrück was a shaping factor in moving this community forward, using the meetings as a place to talk to each other, not just for hacking and chatting on IRC, but for having the structured high-bandwidth conversations which are possible when meeting in person.

Till continued with the story of Akonadi. It was born in 2006 as an ambitious project to overcome the limitations of the architecture of KDE 3's PIM. The idea of separating caching, storing, and managing PIM data from the user interface in form of a server or daemon was floating around in several variants. With Akonadi as an implementation of this idea based on the proven protocols of IMAP for accessing data and D-Bus as a control channel, this idea became reality. Till and Volker showed us screenshots from the first moments when it started to work and we saw data from the Akonadi server in KMail, and Cornelius hinted at the fact that we even had a tool to create Akonadi architecture diagrams. The goals of the project were lofty.

Did Akonadi fail? Well, it's still there today. It's the base for the KDE PIM applications and more, and it's not going away anytime soon. People use it and enjoy it. But did it realize its ambition? Probably not. We discussed that. One conclusion was that the idea of the universal, scalable platform, where people just could plug their application in in a simple way, didn't take into account that that was also adding too much weight to use cases of single applications. We saw that in applications which moved away from Akonadi and went for their own simple backends. The effort to really develop, polish, and maintain such a platform in a stable way for a huge audience was more than what the KDE PIM community could sustain. Would more funding have helped? This sparked an interesting discussion which later was picked up when talking about the future of fat clients.

Thorsten talked about KTimeTracker, which represents the other side of the KDE PIM spectrum, not the big groupware solution, but the simple tool for a specific purpose, tracking time on projects. It has found a new maintainer with Alex nowadays. It's a testament to the frugality and health of a community if projects continue for many years and go from one hand to the other.



After a break which included the successful attempt to create a virtual Osnabrück group picture (you guessed it: we wrote a tool for that), we continued with the second presentation session looking at what is currently in KDE PIM and what is coming.

Bernhard gave an inspiring presentation about the value of the fat client, explaioning how it gives users control, it delivers scalability, it makes it possible to combine data without having to reveal it to cloud providers or other central instances. To all effects, it meets users' real needs. KDE PIM always has been about providing this fat client experience in a big way. But we see that, although there is a need, it's hard to provide sustained funding for the development of fat clients. The data-collecting and ad-based business models of cloud providers don't apply, and not everybody realizes the value that lies in being in control of your own data.

Still, there are quite a few fat client projects, not only in KDE PIM itself, but also around it. Christian presented the state of Kube, an alternative groupware client, partly based on KDE PIM technology, but which also uses its own concepts. It emerged from the Kolab project and is actively developed. It's not meant to cover all possible use cases, but provides a fresh look at how to think of groupware clients.

Volker finally gave a presentation about a new side branch of the KDE PIM community, covering KDE Itinerary and friends. This is about collecting, processing, and presenting transportation information and more in a beautiful and helpful way. As a highlight, Volker showed train and hotel data from the historical Osnabrück events in the new shiny user interface. Starting from extracting tickets and other travel information from emails this has developed into a bunch of new applications and frameworks to help with use cases such as navigating train stations, checking the availability of shared bikes, or reminding of what power plug adaptor you have to bring when you are traveling abroad. This also is based on KDE PIM technology but also reaches out into a new set of technologies and communities, beyond what we expected back in the Osnabrück days. In some way, it developed its own life.

We ended the meeting with favorite beverages, and chatting and discussing everything we had learned. BigBlueButton proved to be a stable and supportive platform for the meeting and its aftermath. We augmented that with a KDE PIM themed Work Adventure room, where people assembled around the Akonadi lamp, or at the KDE kitchen table to chat in smaller groups.

All in all, a really nice way to reconnect, remember and talk about what's cooking in KDE PIM land.
